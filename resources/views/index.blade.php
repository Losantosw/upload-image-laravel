<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Upload Image</title>
</head>
<body>
  if(count($errors) > 0)
      <div class="alert alert-danger">
          Upload Validation Error <br><br>
          <ul>
            foreach($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
      </div>
  @endif
  if($message = Session::get('success'))
      <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">x</button>
          <strong> {{ $message }} </strong>
      </div>
      <img src="/images/{{ Session::get('path') }}" />
  @endif
  <form class="form-horizontal" method='POST' enctype="multipart/form-data" action="{{ url('/upload') }}">
    {{ csrf_field }}
      <fieldset>
        <!-- Form Name -->
        <legend>Upload Image</legend>

        <!-- File Button -->
        <div class="form-group">
          <label class="col-md-4 control-label" for="select_file">File Button</label>
          <div class="col-md-4">
            <input id="filebutton" name="select_file" class="input-file" type="file">
          </div>
        </div>
      </fieldset>
  </form>
</body>
</html>
