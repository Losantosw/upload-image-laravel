<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class UploadController extends Controller
{

    public function index()
    {
        return view('index');
    }

    function upload (Request $request)
    {
        $this->validate($request, [
            'select_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);
        $image = $request->file('select_file');
        $newName = rand() . '.' . $image->
            getClientOriginalExtension();
        $image->(public_path("images"), $newName);

        return back()->with('success', 'image uploaded')->('path', $newName);
    }
}
